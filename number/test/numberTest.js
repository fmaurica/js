import { expect, should } from 'chai';
import * as number from '../number.js';

should();

describe('isEven', function () {
    it('should return true when is even', function () {
        number.isEven(4).should.be.true;
    });

    it('should return fals when is odd', function () {
        number.isEven(5).should.be.false;
    });
});

describe('isPrime', function () {
    it('should throw RangeError if receives negative number', function () {
        expect(function () { number.isPrime(-1); })
            .to
            .throw(RangeError);
    });

    it('should return true when is prime', function () {
        number.isPrime(2).should.be.true;
        number.isPrime(3).should.be.true;
        number.isPrime(13).should.be.true;
        number.isPrime(13).should.be.true;
    });

    it('should return false when is not prime', function () {
        number.isPrime(0).should.be.false;
        number.isPrime(1).should.be.false;
        number.isPrime(4).should.be.false;
        number.isPrime(12).should.be.false;
        number.isPrime(Number.MAX_SAFE_INTEGER).should.be.false;
        number.isPrime(Number.MAX_SAFE_INTEGER - 1).should.be.false;
    });
});
