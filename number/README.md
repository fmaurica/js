Globally install test framework Mocha
and locally install assertion library Chai:
```
$ sudo npm install mocha -g
$ npm install chai
```

Init node module to generate `package.json`: `$ npm init`

Single test: `$ mocha test/numberTest.js`
All tests (of test/ directory by default): `$ mocha`
All tests and waiting for changes to re-test: `$ mocha -w`
Test from VisualCode:
either click on "Debug" in `package.json`
or create `launch.json` for more configurable debugging.

For coverage, use the `c8` testing library
(which is just an interface to the built-in coverage testing library of Node):
```
$ sudo npm install -g c8
$ c8 mocha
```