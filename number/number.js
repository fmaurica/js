export function isEven(n) {
    return n % 2 == 0;
}

export function isPrime(n) {
    if (n < 0) {
        throw RangeError('isPrime expects a non-negative number, but received: ' + n);
    }
    if (n === 0 || n === 1) {
        return false;
    }
    for (let divisor = 2; divisor <= Math.sqrt(n); divisor++) {
        if (n % divisor === 0) {
            return false;
        }
    }
    return true;
}
