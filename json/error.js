export class NotFoundError extends Error {
  constructor (resourceType, message) {
    super(message)
    this.resourceType = resourceType
    this.message = message
  }
}
