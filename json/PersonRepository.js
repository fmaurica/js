import { Person } from './Person.js'
import * as fs from 'fs'
import { NotFoundError } from './error.js'

export class PersonRepository {
  constructor (dbFolderPath) {
    this.dbFolderPath = dbFolderPath
  }

  getByIdSync (id) {
    let person
    try {
      person = fs.readFileSync(this.dbFolderPath + '/' + id + '.json')
    } catch (err) {
      if (err.message.includes('no such file or directory')) {
        throw new NotFoundError(Person, 'id=' + id)
      }
    }
    return JSON.parse(person)
  }

  getById (id, callback) {
    fs.readFile(this.dbFolderPath + '/' + id + '.json', function (err, data) {
      if (err !== null) {
        if (err.message.includes('no such file or directory')) {
          callback(new NotFoundError(Person, 'id=' + id), null)
        } else {
          callback(err, null)
        }
      } else {
        callback(null, JSON.parse(data))
      }
    })
  }
}
