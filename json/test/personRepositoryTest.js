import { assert, should } from 'chai'
import { describe, it } from 'mocha'
import { PersonRepository } from '../PersonRepository.js'
import { Person } from '../Person.js'
import { NotFoundError } from '../error.js'

should()

const personRepository = new PersonRepository('./test/personDb')

describe('getByIdSync', function () {
  it('should throw NotFoundError if id does not exist', function () {
    try {
      personRepository.getByIdSync(2)
      assert.fail('Should be unreachable')
    } catch (e) {
      assert.isOk(e instanceof NotFoundError)
      e.resourceType.should.eql(Person)
      e.message.should.eql('id=2')
    }
  })

  it('should read file correctly if id exists', function () {
    const expected = new Person(1, 'John')

    const actual = personRepository.getByIdSync(1)

    actual.should.eql(expected)
  })
})

describe('getById', function () {
  it('should throw NotFoundError if id does not exist', function (done) {
    personRepository.getById(2, function (e, actual) {
      if (e === null) {
        assert.fail('Should be unreachable')
      }
      assert.isOk(e instanceof NotFoundError)
      e.resourceType.should.eql(Person)
      e.message.should.eql('id=2')
      done()
    })
  })

  it('should read file correctly if id exists', function (done) {
    const expected = new Person(1, 'John')
    personRepository.getById(1, function (e, actual) {
      assert.isOk(e === null)
      actual.should.eql(expected)
      done()
    })
  })
})
