import Person from "../model/Person.js";

export function getAllSync() {
  // https://create-react-app.dev/docs/adding-custom-environment-variables/
  // https://github.com/motdotla/dotenv
  const env = `[${process.env.NODE_ENV}, ${process.env.REACT_APP_NOT_A_SECRET}] `;
  return [new Person(1, env + "John"), new Person(2, env + "Marie")];
}
