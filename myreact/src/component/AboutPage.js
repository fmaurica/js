import React from 'react';

/* historical way of creating components, now favor the new style with functions */
class AboutPage extends React.Component {
    // Following <> is short for <React.Fragment>: allows to avoid unecessary <div>
    render() {
        return (
        <>
            <h1>About</h1>
            <p>Lorem ipsum</p>
        </>);
    }
}

export default AboutPage;
