import React from "react";
import * as PersonRepository from "../repository/PersonRepository.js";

function PersonsPage() {
  const [persons, setPersons] = React.useState([]);

  React.useEffect(() => setPersons(PersonRepository.getAllSync()), []);

  return (
    <table className="table">
      <thead>
        <tr>
          <th>Id</th>
          <th>Name</th>
        </tr>
      </thead>
      <tbody>
        {persons.map((person) => (
          <tr key={person.id}>
            <td>{person.id}</td>
            <td>{person.name}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

export default PersonsPage;
