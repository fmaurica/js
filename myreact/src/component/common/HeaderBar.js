import React from "react";

function HeaderBar() {
  return (
    <nav>
      <a href="/">Home</a> | <a href="/persons">Persons</a> |{" "}
      <a href="/about">About</a>
    </nav>
  );
}

export default HeaderBar;
