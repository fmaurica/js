import React from "react";
import HeaderBar from "./common/HeaderBar.js";
import HomePage from "./HomePage.js";
import AboutPage from "./AboutPage.js";
import PersonsPage from "./PersonsPage.js";

function App() {
  function getPage() {
    const route = window.location.pathname;
    if (route === "/about") return <AboutPage />;
    if (route === "/persons") return <PersonsPage />;
    else return <HomePage />;
  }

  return (
    <div className="container-fluid">
      <HeaderBar />
      {getPage()}
    </div>
  );
}

export default App;
