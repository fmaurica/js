import 'bootstrap/dist/css/bootstrap.min.css'; // importing CSS in js file is possible thanks to Webpack
import React from 'react';
import { render } from 'react-dom';
import App from './component/App.js';

render(<App />, document.getElementById('root'));
